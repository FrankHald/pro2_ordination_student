package gui;

import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import ordination.Laegemiddel;
import service.Service;

public class StatistikPane extends GridPane {
	private TextField ordinationerPerVagtPerLagemiddel = new TextField();
	private TextField txfVagtFra = new TextField();
	private TextField txfVagtTil = new TextField();
	private ComboBox<Laegemiddel> lstLagemidler = new ComboBox<Laegemiddel>();
	private Service service;

	public StatistikPane() {
		service = Service.getService();
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);
		initContent();
	}

	private void initContent() {
		GridPane pane1 = new GridPane();
		pane1.setHgap(20);
		pane1.setVgap(10);
		pane1.setPadding(new Insets(10));

		GridPane pane2 = new GridPane();
		pane2.setHgap(20);
		pane2.setVgap(10);
		pane2.setPadding(new Insets(10));

		pane1.setStyle("-fx-border-color: grey;");
		pane2.setStyle("-fx-border-color: grey;");

		Label label = new Label("Antal ordinationer");
		label.setFont(new Font(25));
		this.add(label, 0, 0, 2, 1);

		txfVagtFra.setMaxWidth(40);
		txfVagtTil.setMaxWidth(40);
		pane1.add(new Label("Vægt fra: "), 0, 0);
		pane1.add(txfVagtFra, 1, 0);

		pane1.add(new Label("Vægt til: "), 0, 1);
		pane1.add(txfVagtTil, 1, 1);

		lstLagemidler.getItems().setAll(service.getAllLaegemidler());
		pane1.add(new Label("Lægemiddel: "), 0, 2);
		pane1.add(lstLagemidler, 1, 2);
		this.add(pane1, 0, 1);

		pane2.add(new Label("Antal: "), 0, 0);
		ordinationerPerVagtPerLagemiddel.setEditable(false);
		pane2.add(ordinationerPerVagtPerLagemiddel, 1, 0);
		this.add(pane2, 0, 2);

		// Adding listeners
		txfVagtFra.setOnKeyReleased(event -> updateDetails());
		txfVagtTil.setOnKeyReleased(event -> updateDetails());
		lstLagemidler.setOnAction(event -> updateDetails());

		updateDetails();
	}

	public void updateDetails() {
		try {
			int vFra = Integer.valueOf(txfVagtFra.getText());
			int vTil = Integer.valueOf(txfVagtTil.getText());
			Laegemiddel lagemiddel = lstLagemidler.getSelectionModel()
					.getSelectedItem();
			int antal = service.antalOrdinationerPrVagtPrLagemiddel(vFra, vTil,
					lagemiddel);
			ordinationerPerVagtPerLagemiddel.setText(antal + "");
		} catch (NumberFormatException e) {
			ordinationerPerVagtPerLagemiddel.setText("");
		}
	}

}
