package tests;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import service.Service;

public class PNTest {

	PN pn;
    LocalDate startDen;
    LocalDate slutDen;
    Laegemiddel laegemiddel;
    Patient jane;
    
    
	@Before 
	public void setUp(){
	
	jane = new Patient("121256-0512", "Jane Jensen", 63.4);
	laegemiddel =  new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
	pn = new PN(LocalDate.of(2017, 02, 06), LocalDate.of(2017, 02, 16), 2.0, jane);
	}
	
	@Test
	public void testSetLaegeMiddel() {
		pn.setLaegemiddel(laegemiddel);
		assertEquals(pn.getLaegemiddel(), laegemiddel);
	}
	
	@Test
	public void testGetType(){
		assertEquals("PN", pn.getType());
	}
	
	@Test
	public void testGetDatoer(){
		pn.givDosis(LocalDate.of(2017, 02, 10));
		pn.givDosis(LocalDate.of(2017, 02, 12));
		assertTrue(pn.getDatoer().contains(LocalDate.of(2017, 02, 10)));
		assertTrue(pn.getDatoer().contains(LocalDate.of(2017, 02, 12)));
	}
	
	@Test
	public void testGivDosis(){
		pn.givDosis(LocalDate.of(2017, 02, 10));
		assertTrue(pn.getDatoer().contains(LocalDate.of(2017, 02, 10)));
		assertFalse(pn.getDatoer().contains(LocalDate.of(2017, 02, 20)));
		assertFalse(pn.getDatoer().contains(LocalDate.of(2017, 02, 01)));
	}
	
	@Test
	public void testGetAntalEnheder(){
		assertEquals(2.0, pn.getAntalEnheder(), 0.00001);
	}
	
	@Test
	public void getAntalGangeGivet(){
		pn.givDosis(LocalDate.of(2017, 02, 10));
		pn.givDosis(LocalDate.of(2017, 02, 12));
		assertEquals(2, pn.getDatoer().size());
	}
	
	@Test
	public void testSamletDosis(){
		pn.givDosis(LocalDate.of(2017, 02, 10));
		pn.givDosis(LocalDate.of(2017, 02, 11));
		pn.givDosis(LocalDate.of(2017, 02, 12));
		pn.givDosis(LocalDate.of(2017, 02, 13));
		pn.givDosis(LocalDate.of(2017, 02, 14));
		assertEquals(10, pn.samletDosis(), 0.00001);
	}
	
	@Test
	public void testDoegnDosis(){
		pn.givDosis(LocalDate.of(2017, 02, 10));
		pn.givDosis(LocalDate.of(2017, 02, 11));
		pn.givDosis(LocalDate.of(2017, 02, 12));
		pn.givDosis(LocalDate.of(2017, 02, 13));
		pn.givDosis(LocalDate.of(2017, 02, 14));
		assertEquals(1.0, pn.doegnDosis(), 0.00001);
	}

    @Test
    public void testDoegnDosis2(){
        pn = new PN(LocalDate.of(2017, 02, 06), LocalDate.of(2017, 02, 16), 3.0, jane);
        pn.givDosis(LocalDate.of(2017, 02, 10));
        pn.givDosis(LocalDate.of(2017, 02, 11));
        pn.givDosis(LocalDate.of(2017, 02, 12));
        pn.givDosis(LocalDate.of(2017, 02, 13));
        pn.givDosis(LocalDate.of(2017, 02, 14));
        assertEquals(1.5, pn.doegnDosis(), 0.00001);
    }
}
