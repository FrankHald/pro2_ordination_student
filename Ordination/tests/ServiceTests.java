package tests;

import ordination.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import service.Service;

import java.time.LocalDate;
import java.time.LocalTime;

import static junit.framework.TestCase.*;


public class ServiceTests {

    Service service;
    LocalDate startDen;
    LocalDate slutDen;
    Laegemiddel laegemiddel;
    Patient jane;
    LocalTime[] klokkeSlet = {LocalTime.of(8,0), LocalTime.of(10,0), LocalTime.of(12,0), LocalTime.of(20,0)};
    double[] antalEnheder = {1,2,3,4};

    @Before
    public void setUp() {
        service = Service.getTestService();
        startDen = LocalDate.of(2017, 02, 06);
        slutDen = LocalDate.of(2017, 02, 16);
        laegemiddel = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
        jane = new Patient("121256-0512", "Jane Jensen", 63.4);

    }

    @Test
    public void testOpretPNOrdination() {
        PN pn = service.opretPNOrdination(startDen, slutDen, jane, laegemiddel, 2);
        assertNotNull(pn);
        assertSame(pn.getLaegemiddel(), laegemiddel);
        assertEquals(pn.getAntalEnheder(), 2.0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretPNOrdination2() {
        service.opretPNOrdination(slutDen, startDen, jane, laegemiddel, 2);
    }

    @Ignore
    @Test(expected = IllegalArgumentException.class)
    public void testOpretPNOrdination3() {
        service.opretPNOrdination(startDen, slutDen, jane, laegemiddel, -2);
    }

    @Test
    public void testOpretDagligFastOrdination() {
        DagligFast dagligFast = service.opretDagligFastOrdination(startDen, slutDen, jane, laegemiddel, 1,1,1,1);
        assertNotNull(dagligFast);
        assertSame(dagligFast.getLaegemiddel(), laegemiddel);
        assertEquals(dagligFast.samletDosis(), 4.0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligFastOrdination2() {
        service.opretDagligFastOrdination(slutDen, startDen, jane, laegemiddel, 1,1,1,1);
    }

    @Ignore
    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligFastOrdination3() {
        service.opretDagligFastOrdination(startDen, slutDen, jane, laegemiddel, -1,0,0,0);
    }

    @Test
    public void testOpretDagligSkaevOrdination() {
        DagligSkaev dagligSkaev = service.opretDagligSkaevOrdination(startDen, slutDen, jane, laegemiddel, klokkeSlet, antalEnheder);
        assertNotNull(dagligSkaev);
        assertSame(dagligSkaev.getLaegemiddel(), laegemiddel);
        assertEquals(dagligSkaev.doegnDosis(), 10.0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligSkaevOrdination2() {
        service.opretDagligSkaevOrdination(slutDen, startDen, jane, laegemiddel, klokkeSlet, antalEnheder);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligSkaevOrdination3() {
        double[] antalEnheder2 = {1,2};
        service.opretDagligSkaevOrdination(startDen, slutDen, jane, laegemiddel, klokkeSlet, antalEnheder2);
    }

    @Ignore
    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligSkaevOrdination4() {
        double[] antalEnheder2 = {-1,2,3,4};
        service.opretDagligSkaevOrdination(startDen, slutDen, jane, laegemiddel, klokkeSlet, antalEnheder2);
    }

    @Test
    public void testOrdinationPNAnvendt() {
        PN pn = service.opretPNOrdination(startDen, slutDen, jane, laegemiddel, 2);
        service.ordinationPNAnvendt(pn, LocalDate.of(2017, 02, 10));
        assertTrue(pn.getDatoer().contains(LocalDate.of(2017, 02, 10)));
    }

    @Test
    public void testAnbefaletDosisPrDoegn() {
        Patient test = new Patient("123423-1234", "Test", 20);
        Laegemiddel laegemiddel2 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
        assertEquals(service.anbefaletDosisPrDoegn(test, laegemiddel2), 20.0);
    }

    @Test
    public void testAnbefaletDosisPrDoegn2() {
        Patient test = new Patient("123423-1234", "Test", 25);
        Laegemiddel laegemiddel2 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
        assertEquals(service.anbefaletDosisPrDoegn(test, laegemiddel2), 37.5);
    }

    @Test
    public void testAnbefaletDosisPrDoegn3() {
        Patient test = new Patient("123423-1234", "Test", 50);
        Laegemiddel laegemiddel2 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
        assertEquals(service.anbefaletDosisPrDoegn(test, laegemiddel2), 75.0);
    }

    @Test
    public void testAnbefaletDosisPrDoegn4() {
        Patient test = new Patient("123423-1234", "Test", 120);
        Laegemiddel laegemiddel2 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
        assertEquals(service.anbefaletDosisPrDoegn(test, laegemiddel2), 180.0);
    }

    @Test
    public void testAnbefaletDosisPrDoegn5() {
        Patient test = new Patient("123423-1234", "Test", 200);
        Laegemiddel laegemiddel2 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
        assertEquals(service.anbefaletDosisPrDoegn(test, laegemiddel2), 400.0);
    }

    @Test
    public void testAntalOrdinationerPrVagtPrLaegemiddel() {
        Patient lone = service.opretPatient("123423-1234", "Lone", 20);
        Patient carsten = service.opretPatient("123423-1234", "Carsten", 22);
        Patient mads = service.opretPatient("123423-1234", "Mads", 70);
        service.opretPNOrdination(startDen, slutDen, lone, laegemiddel, 2);
        service.opretPNOrdination(startDen, slutDen, lone, laegemiddel, 2);
        service.opretPNOrdination(startDen, slutDen, carsten, laegemiddel, 2);
        service.opretPNOrdination(startDen, slutDen, mads, laegemiddel, 2);
        service.opretPNOrdination(startDen, slutDen, mads, laegemiddel, 2);
        Laegemiddel laegemiddel2 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");

        assertEquals(service.antalOrdinationerPrVagtPrLagemiddel(19, 25, laegemiddel), 3);
        assertEquals(service.antalOrdinationerPrVagtPrLagemiddel(20, 25, laegemiddel), 1);
        assertEquals(service.antalOrdinationerPrVagtPrLagemiddel(19, 100, laegemiddel), 5);
        assertEquals(service.antalOrdinationerPrVagtPrLagemiddel(10, 200, laegemiddel2), 0);
    }

}
