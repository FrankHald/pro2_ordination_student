package tests;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Patient;
import org.junit.Before;
import org.junit.Test;
import service.Service;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.Assert.assertEquals;

public class DosisTests {
    Service service;
    LocalDate startDen;
    LocalDate slutDen;
    Laegemiddel laegemiddel;
    Patient jane;

    @Before
    public void setUp() {
        service = Service.getTestService();
        startDen = LocalDate.of(2017, 02, 06);
        slutDen = LocalDate.of(2017, 02, 16);
        laegemiddel = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
        jane = new Patient("121256-0512", "Jane Jensen", 63.4);
    }

    @Test
    public void testDagligSkavDoegnDosis() {
        LocalTime[] klokkeSlet = {LocalTime.of(8,0), LocalTime.of(10,0), LocalTime.of(12,0), LocalTime.of(20,0)};
        double[] antalEnheder = {1,2,3,4};
        DagligSkaev dagligSkaev = new DagligSkaev(startDen, slutDen, jane, antalEnheder, klokkeSlet);
        assertEquals(dagligSkaev.doegnDosis(), 10.0, 0.0001);
    }

    @Test
    public void testDagligSkavDoegnDosis2() {
        LocalTime[] klokkeSlet = {LocalTime.of(8,0), LocalTime.of(10,0), LocalTime.of(12,0), LocalTime.of(20,0)};
        double[] antalEnheder = {0,0,0,5};
        DagligSkaev dagligSkaev = new DagligSkaev(startDen, slutDen, jane, antalEnheder, klokkeSlet);
        assertEquals(dagligSkaev.doegnDosis(), 5.0, 0.0001);
    }

    @Test
    public void testDagligFastDoegnDosis() {
        DagligFast dagligFast = new DagligFast(startDen, slutDen, 1, 2, 3, 4, jane);
        assertEquals(dagligFast.doegnDosis(), 10.0, 0.001);
    }
}
