package tests;

import static org.junit.Assert.*;
import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;

import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.PN;
import ordination.Patient;
import ordination.Dosis;
import ordination.DagligFast;
import ordination.DagligSkaev;

import org.junit.Test;

public class TestAssocieringer {
	
	Laegemiddel laegemiddel;
	Ordination ordination;
	Patient jane;
	Dosis dosis;
	DagligFast dagligFast;
	DagligSkaev dagligSkaev;
	
	@Before
	public void setUp(){
		laegemiddel = new Laegemiddel("Fucidin", 1.0, 1.0, 1.0, "Styk");
		jane = new Patient("121256-0512", "Jane Jensen", 63.4);
		
	}
	
	@Test
	public void testOrdinationLaegemiddel() {
		ordination = new PN(LocalDate.of(2017, 02, 06), LocalDate.of(2017, 02, 16), 2, jane);
		ordination.setLaegemiddel(laegemiddel);
		assertEquals(ordination.getLaegemiddel(), laegemiddel);
	}
	
	@Test
	public void testPatientOrdination(){
		ordination = new PN(LocalDate.of(2017, 02, 06), LocalDate.of(2017, 02, 16), 2, jane);
		assertTrue(jane.getOrdinationer().contains(ordination));
	}
	
	@Test 
	public void testDosisDagligSkaev(){
		LocalTime[] klokkeSlet = {LocalTime.of(8,0), LocalTime.of(10,0), LocalTime.of(13,0), LocalTime.of(20,0)};
        double[] antalEnheder = {1,2,3,4};
        LocalTime localTime = LocalTime.of(12, 00);
		dagligSkaev = new DagligSkaev(LocalDate.of(2017, 02, 06), LocalDate.of(2017, 02, 16), jane, antalEnheder, klokkeSlet);
		dagligSkaev.opretDosis(localTime, 4);
		
		for (Dosis d : dagligSkaev.getDoser()) {
			if(d.getTid() == localTime.of(12, 00) && d.getAntal() == 4){
				assertEquals(d.getTid(), localTime);
				assertEquals(d.getAntal(), 4.0, 0.01);				
			}
		}
	}

}
