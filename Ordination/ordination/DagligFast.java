package ordination;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

public class DagligFast extends Ordination {
	
	private Dosis[] doser = new Dosis[4];
	
    public DagligFast(LocalDate startDen, LocalDate slutDen,
            double morgenAntal, double middagAntal, double aftenAntal,
            double natAntal, Patient patient) 
    {
		super(startDen, slutDen, patient);
		doser[0] = createDosis(LocalTime.of(07, 00), morgenAntal);
		doser[1] = createDosis(LocalTime.of(12, 00), middagAntal);
		doser[2] = createDosis(LocalTime.of(18, 00), aftenAntal);
		doser[3] = createDosis(LocalTime.of(23, 00), natAntal);
	}
    
	public Dosis[] getDagligFasteDoser(){
		return Arrays.copyOf(doser, 4);
	}
	
	public Dosis createDosis(LocalTime tid, double antal){
		Dosis dosis = new Dosis(tid, antal);
		return dosis;
	}

    public Dosis[] getDoser() {
        return doser;
    }

    @Override
	public double samletDosis() {
		double samletAntal = 0;
		for (int i = 0; i < doser.length; i++) {
			samletAntal+=doser[i].getAntal();
		}
		return samletAntal * antalDage();
	}

	@Override
	public double doegnDosis() {
		return samletDosis()/antalDage();
	}

	@Override
	public String getType() {
		return "Daglig Fast";
	}
	
	
}


