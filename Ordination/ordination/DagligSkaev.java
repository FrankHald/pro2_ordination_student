package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> doser = new ArrayList<>();
	
	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Patient patient, double[] antalEnheder, LocalTime[] klokkeSlet) {
		super(startDen, slutDen, patient);
        for (int i = 0; i < antalEnheder.length; i++) {
            opretDosis(klokkeSlet[i], antalEnheder[i]);
        }
	}

	public ArrayList<Dosis> getDoser() {
		return doser;
	}

	public void setTidspunkter(ArrayList<Dosis> doser) {
		this.doser = doser;
	}

    public void opretDosis(LocalTime tid, double antal) {
        Dosis dosis = new Dosis(tid,antal);
        doser.add(dosis);
    }

	@Override
	public double samletDosis() {
		double sum = 0;
		for(int i = 0; i < doser.size(); i++){
			sum += doser.get(i).getAntal();
			
		}
		return sum * antalDage();
	}

	@Override
	public double doegnDosis() {
		return this.samletDosis() / this.antalDage();
	}

	@Override
	public String getType() {
		
		return "DagligSkæv";
	} 

	
}
