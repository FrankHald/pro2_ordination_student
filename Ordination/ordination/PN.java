package ordination;

import java.time.LocalDate;
import java.util.ArrayList;

import static java.time.temporal.ChronoUnit.DAYS;

public class PN extends Ordination {

    private double antalEnheder;
    private ArrayList<LocalDate> datoer = new ArrayList<>();


    public PN(LocalDate startDen, LocalDate slutDen, double antalEnheder, Patient patient) {
        super(startDen, slutDen, patient);
        this.antalEnheder = antalEnheder;
    }

    /**
     * Registrerer at der er givet en dosis paa dagen givesDen
     * Returnerer true hvis givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
     * Retrurner false ellers og datoen givesDen ignoreres
     * @param givesDen
     * @return
     */
    public boolean givDosis(LocalDate givesDen) {
        return givesDen.isAfter(getStartDen()) && givesDen.isBefore(getSlutDen()) && datoer.add(givesDen);
    }

    public double doegnDosis() {
        long days = DAYS.between(getStartDen(), getSlutDen());
        return samletDosis()/days;
    }

    public double samletDosis() {
        return getAntalGangeGivet() * antalEnheder;
    }

    /**
     * Returnerer antal gange ordinationen er anvendt
     * @return
     */
    public int getAntalGangeGivet() {
        return datoer.size();
    }

    public double getAntalEnheder() {
        return antalEnheder;
    }

    public ArrayList<LocalDate> getDatoer() {
        return new ArrayList<>(datoer);
    }

    @Override
    public String getType() {
        return "PN";
    }

}
